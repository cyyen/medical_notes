# Blood drawing order

* Blood order of draw in the United States determined by the Clinical Laboratory Standards Institute.
* Order of draw:
  1. yellow (blood culture): sodium polyanethole sulfonate (SPS)
  2. light blue (coagulation tests): sodium citrate
  3. plain red (routine venipuncture): no additive
  4. plastic red (biochemistry, immunology, serology, cross-matching): clot activator
  5. gold (biochemistry, immunology, serology): serum separator tube with clot activator
  6. green (biochemistry, chromosomal analysis): heparin lithium or heparin sodium
  7. lavender (cross-matching, hematology, flow cytology, molecular diagnosis): ethylenediaminetetraacetic acid (EDTA)
  8. gray (biochemistry requiring inhibition of glycolysis): sodium fluoride or potassium oxalate
* Order of draw must be enforced to prevent additive cross-contamination
  * examples: EDTA contains potassium -> place after tests for potassium; clot activator tubes interfere with coagulation profile -> place after coagulation profile tests
  * mnemonic: Sally Brought Really Great Grease and Left Gravy
    * sterile, blue, red, gold, green, lavender, gray
