import os
import json
import re

filelist = [os.path.join(i[0], filename) for i in os.walk('.') for filename in i[2] if filename.endswith('.md')]
tipuecontent = {'pages': []}

for f in filelist:
    print(f)
    with open(f) as article:
        title = re.search('\w+', article.readline().replace('\n', '')).group()
        tipuecontent['pages'].append({'title': title, 'text': 
article.read().replace('\n',' '), 'tags': '', 'url':  'index.html#!' + 
os.path.relpath(f)})

print(tipuecontent)

with open('tipuesearch/tipuesearch_content.js', mode='w') as fh:
    fh.write('var tipuesearch = ')
    fh.write(json.dumps(tipuecontent))
    fh.write(';')
