# Potassium binders

## Calcium polystyrene sulfonate (Kalimate)

* usual dose: 15-30g QD PO (UpToDate: 15g TID-QID PO)
* effect: 1.36-1.82 mEq/g

## Sodium polystyrene sulfonate (Kuzem)

* usual dose: 15g TID-QID PO (UpToDate: 15g QD-QID PO)
* effect: 1 mEq/g
