# Anticoagulants

## Mechanisms

* Direct thrombin inhibitors (factor IIa)
  * dabigatran
* Factor Xa inhibitors
  * rivaroxaban
* Vitamin K antagonists
  * coumarins
    * warfarin

## Antidotes

* warfarin: vitamin K, FFP
* dabigatran: idarucizumab
* factor Xa inhibitors (if imminent risk of death): andexanet alfa, 4-factor PCC (prothroombin complex concentrate)