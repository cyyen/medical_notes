# Antiplatelet agents

## Mechanisms

* Glycoprotein IIb/IIIa antagonists
  * same mechanism as Glanzmann's thrombasthenia
  * drugs in class
    * abciximab
      * monoclonal antibody (Fab fragment)
    * eptifibatide
    * tirofiban
