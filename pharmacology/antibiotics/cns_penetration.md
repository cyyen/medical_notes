# Penetration of central nervous system by antibiotics

* Ideal physical characteristics:
  * small molecule
  * mildly lipophilic
  * not a strong ligand of drug efflux pumps

* Antibiotics with good CNS penetration
  * isoniazid
  * pyrazinamide
  * metronidazole
  * fluoroquinolones (certain types)

* Antifungals with good CNS penetration
  * fluconazole
