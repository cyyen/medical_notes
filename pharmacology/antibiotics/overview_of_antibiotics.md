# Overview of antibiotics

## Gram-positive cocci

* Streptococci: 1st choice: oxacillin or cefazolin
* Staphylococci: 1st choice: penicillin G
* Enterococci: 1st choice: ampicillin
* penicillin resistance -> vancomycin
* vancomycin resistance: daptomycin, linezolid
  * linezolid: risk of marrow suppression
  * Enterococcus gallinarum: intrinsic resistance to vancomycin; ampicillin adequate

## Gram-negative bacilli

* E. coli, Klebsiella pneumoniae, Proteus mirabilis
* Haemophilus influenzae, Moraxella catarrhalis
* Neisseria meningitidis, Nesseria gonorrhea
* Stenotrophomonas maltophila, Citrobacter freundii, Morganella moganii
* Pseudomonas aeruginosa, Burkholderia capaciae
* Acinetobacter baumanii, Fusobacterium necrophorum 

## Penicillin G

* Use cases:
  * Streptococci
  * Neisseria meningitidis
  * Actinomyces israelii
  * Treponema pallidum (only choice for neurosyphilis, vertically-transmitted syphilis)
* Dosing: 3 MU q4h (max dose 24 MU per day; risk of seizure if overdose)
  * renal adjustment needed
* Drawback: short half-life, risk of phlebitis

## Oxacillin

* Dosing: 2g q4h
  * no renal adjustment needed

## Amphotericin B

* No renal adjustment needed despite nephrotoxicity
  * self-elimination by Hoffman reaction

## Unasyn

* Problems with using Unasyn for cellulitis: dose limited by sulbactam
  * e.g., maximum dose is 3g q6h, but cellulitis requires ampicillin 2g q4h; this results in an effective dose of ampicillin of 2g q6h when using Unasyn, which is inadequate
  * sulbactam inhibit beta-lactamases of GNBs
  * primary choice: cefazolin 2g q8h or penicillin G
* Unasyn and Augmentin suitable against anaerobes

## Clindamycin

* Penicillin + clindamycin: immunomodulatory effect

## Piperacillin

* Dosage: 4g q6h
* Modified with polar functional groups
* Tazocin: tazobactam added

## ESBL

* primary choice: carbapenems

## Cefepime

* MIC=2-8: prognosis worse than cases where MIC &lt; 2

## Cephalosporins

* Resistant: MRSA, Enterococci, Listeria
* Ceftazidime: activity against Pseudomonas compared with other 3rd-gen cephalosporins but reduced activity against GPCs
* Cefazolin: adequate for most UTIs

## Cephamycins

* Flomoxef, cefmetazole, cefoxitin
* Anaerobe coverage, ESBL coverage
* Cefmetazole, cefoxitin: contain NMTT side chains, causing disulfiram-like reactions and PT-INR prolongation

## Quinolones

* ciprofloxacin: 400mg q12h IV or 500mg q12h (400mg q8h IV for Pseudomonas, or 750mg bidac PO)
* levofloxacin: 500mg qd (750mg qd for Pseudomonas); high bioavailability; respiratory quinolone
* moxifloxacin: good against GPCs, Enterococci, anaerobes; poor activity against Pseudomonas; respiratory infections, IAIs, soft tissue infections
* gemifloxacin: no activity against mycobacteria

## Carbapenems

* ertapenem: no activity against Pseudomonas
* imipenem: no activity against VRE; good against Acinetobacter, PRSP; used in conjunction with cilastatin for prevention of renal metabolism; seizure risk
* meropenem: no activity against VRE; good against Pseudomonas, CNS infections, PRSP
* doripenem

## Tigecycline

* glycylcyclines
* Good against ESBL, VRE, MRSA, PRSP
* Used in polymicrobial, complicated IAIs
* Not suitable for bloodstream infections (low effective concentration), Pseuodomonas, Proteus, Providentia
* No renal adjustment
* Loading: 100mg, then 50mg qd for maintenance

## Fosfomycin

* Covers Pseudomonas, VRE
* Good for UTI
