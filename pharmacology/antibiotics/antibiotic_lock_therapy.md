# Antibiotic lock therapy

## Rationale

* Catheter-related bloodstream infections are associated with biofilm formation on catheter surface, and requires a 100-1000x increase in antibiotic concentration to kill
* If catheter removal is infeasible, antibiotic lock therapy may be considered
* Nb. only weak evidence exists for the effectiveness of lock therapy

## Method

* Select proper antibiotic and amount of heparin to add
* Know the catheter's lumen volume and other caveats (e.g., if catheter is compatible with the regimen)
* Inject lock antibiotic mixture and hold it in the lumen for a specified amount of time
* Draw mix away after specified time is up
