# TNF antagonists

## Types

### Anti TNF-alpha monoclonal antibodies

* Certolizumab pegol (trade name: Cimzia)
  * pegylated Fab fragment (Fc removed)
    * decreased penetration into placenta in pregnant patients
      * transplacental movement is mainly mediated by Fc in 1st and 2nd trimesters; transplacental effects of certolizumab pegol start to increase by the 3rd trimester
  * indications
    * United States: Crohn's disease, rheumatoid arthritis, psoriatic arthritis, ankylosing spondylitis
    * Taiwan: moderate to severe rheumatoid arthritis not responsive to one or more DMARDs (e.g., methotrexate)
* Adalimumab (trade name: Humira)
* Infliximab (trade name: Remicade)
* Golimumab (trade name: Simponi)

### Decoy receptors

* Etanercept (trade name: Enbrel)
