# Equianalgesic table

Note: This table contains only analgesics that are available at NCKUH. Doses are those for 65kg adults. Certain drugs found at NCKUH (etodolac, flurbiprofen, meloxicam, tiaprofenic acid, celecoxib, etoricoxib) have been omitted because equianalgesic data is not available.

Tip: When *switching* between opioids, multiply *equivalent dose of new opioid* by 0.5-0.75 to account for incomplete cross-tolerance (usually given as 0.25-0.5).

| Analgesic               | Streng. | Usu. dose       | PO morphine equiv. | Route | Max dose  | Note |
|-------------------------|---------|-----------------|--------------------|-------|-----------|------|
| Indomethacin            | 1/64    | 50mg bid        | 0.78mg bid         | SUPP  | 100mg qd? |      |
| Acetaminophen           | 1/360   | 325-650mg q4-6h | 0.9-1.8mg q4-6h    | PO    | 4000mg qd | 1    |
| Lysine acetylsalicylate | 1/648?  | 0.9-3.6g qd     | 1.4-5.6mg qd?      | IM,IV | 7.2g qd?  | 2    |
| Aspirin                 | 1/360   | 650mg q4-6h     | 1.8mg q4-6h        | PO    | 4000mg qd | 3    |
| Ibuprofen               | 1/222   | 400mg q4-6h     | 1.8mg q4-6h        | PO    | 3200mg qd |      |
| Naproxen                | 1/138   | 250mg q6-8h     | 1.8mg q6-8h        | PO    | 1.25g qd  |      |
| Diclofenac              | 1/14    | 50mg bid-tid    | 3.57mg bid-tid     | PO    | 200mg qd  |      |
| Codeine                 | 30/200  | 30-60mg q4-6h   | 4.5-9mg q4-6h      | PO    | 360mg qd  | 4    |
| Alfentanil              | 10-25   | 7-15mcg/kg q10m | 4.55-9.75mg q10m   | IV    |           | 5    |
| Tramadol                | 1/10    | 50-100mg q4-6h  | 5-10mg q4-6h       | PO,IV | 400mg qd  | 6    |
| Morphine                | 1       | 5-30mg q4h      | 5-30mg q4h         | PO    | 180mg qd? |      |
| Morphine                | 3-4     | 2-5mg q3-4h     | 6-20mg q3-4h       | IV,SC | 120mg qd? |      |
| Fentanyl                | 50-100  | 2mcg/kg bolus   | 6.5-13mg bolus     | IV    | ?         | 7    |
| Buprenorphine           | 37.5    | 0.2-0.4mg q6-8h | 7.5-15mg q6-8h     | SL    | 1.6mg qd? |      |
| Ketorolac               | 2/5     | 30mg q6h        | 12mg q6h           | IV    | 120mg qd  | 8    |
| Meperidine              | 1/3     | 50-150mg q3-4h  | 16.7-50mg q3-4h    | PO,IV | 400mg qd? | 9    |
| Nefopam                 | 5/8     | 30mg qid        | 18.75mg qid        | PO    | 300mg qd  | 10   |
| Butorphanol             | 15      | 2mg q2-3h       | 30mg q2-3h         | NS    | 360mg qd? | 11   |
| Fentanyl                | 50-100  | 2.5mg q3d       | 90-180mg q3d       | TD    | 0.83mg qd?| 12   |

1: Decrease maximum daily acetaminophen dose to 2000-3000mg in known or suspected liver impairment
2: 0.9g of lysine acetylsalicylate equivalent to 0.5g aspirin according to NCKUH Formulary 12e; *strength assumes aspirin given PO and IV are equipotent*; *max dose based on that for oral aspirin*
3: Oral aspirin no longer recommended as analgesic
4: **Regulated**; codeine potency based on equianalgesic chart in NCKUH Formulary 12e
5: **Regulated**; alfentanil used for surgical procedures only; increase bolus dose if duration of surgical procedure is longer than 10min
6: Tramadol contraindicated in users of MAOIs; has potential for seizures; beware of acetaminophen overdose when using tramadol/acetaminophen compound
7: **Regulated**; IV fentanyl used only as adjunct to general anesthesia; dosage shown is for minor surgery (dosage for major surgery may reach as high as 50mcg/kg); maintenance dose is 25-50mg but interval between doses is not given in formulary
8: Ketorolac strength based on American Pain Society guidelines 2008; *not for use as antipyretic*
9: **Regulated**; meperidine contraindicated in users of MAOIs; potential for seizures
10: Nefopam contraindicated in users of MAOIs or TCAs; potential for hypertensive crisis or serotonin syndrome
11: **Regulated**; max initial dose of butorphanol is 1mg in elderly and hepatically- or renally-impaired patients
12: **Regulated**; dosage of transdermal fentanyl should be based on prior oral morphine; 2.5mg patch releases 25mcg/hr (1.8mg q3d) (PO morphine equivalent here is calculated from this release rate); bioavailability is 92%