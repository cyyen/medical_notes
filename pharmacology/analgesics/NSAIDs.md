# NSAIDs

* Mechanism of action: cyclooxygenase (COX) inhibition
  * no studies exist relating the degree of COX inhibition with anti-inflammatory efficacy
  * However, effect of COX inhibition on inflammation not completely understood

---

# NSAID poisoning

* General approach: obtain NSAID type, amount, time of ingestion and co-ingestants (e.g., acetaminophen, aspirin)
* Clinical manifestations: nausea, vomiting, dizziness, drowsiness, blurred vision; however, only &lt; 0.5% experienced severe harm
  * Acid-base abnormalities
    * high anion gap metabolic acidosis
  * Acute renal failure
    * rare; increased serum creatinine, decreased urine output, hyperkalemia
    * usually reversible upon discontinuation
  * Cardiovascular toxicity
    * refractory hypotension
  * CNS toxicity
    * seizures
    * mechanism unknown
  * Hematologic toxicity
    * aplastic anemia, agranulocytosis
  * Allergies
* Testing
  * NSAID levels: poor correlation with symptoms, not clinically useful
  * acetaminophen, salicylates: to check for co-ingestion
  * if symptomatic:
    * renal function, electrolytes: baseline
    * glucose: to rule out hypoglycemia in consciousness change
    * EKG
    * urine pregnancy test
    * CBC if bleeding
    * ABG if significant overdose (i.e., &gt; 6g in adults)
* Management
  * airway, breathing, circulation
    * apnea and hypotension are rare
    * intubation and mechanical ventilation if necessary
  * gastrointestinal decontamination
    * single-dose activated charcoal unless contraindicated
  * supportive care
    * sodium bicarbonate: lack of evidence for use
  * hemodialysis: ineffective (NSAIDs exhibit high protein binding)
  
## Ibuprofen poisoning

* Metabolism: via hepatic transformation and urinary excretion
* Half-life: &lt; 8hr

### Children and adolescents

* Toxic dose usually &gt; 400mg / kg lean body weight

### Adults

* Toxic dose usually &gt; 6g