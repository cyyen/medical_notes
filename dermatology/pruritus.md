# Pruritus

## Etiology

* dermatitis (atopic, contact, ...)
* urticaria
* sunburns
* insect bites
* eczema

Causes related to internal medicine:

* uremia (most common)
* cholestasis
* psoriasis
* allergies
* infections (rash, e.g., chickenpox)

## Treatment

### Topical

* emollients
* corticosteroids
* antihistamines
* local anesthetics
* counterirritants (e.g., menthol oil)
* astringents (e.g., zinc oxide)

### Systemic

* antidepressants (e.g., SSRI, TCA)
  * doxepin [UpToDate 2017]
* antihistamines
* nalfurafine (for uremic pruritus only)
