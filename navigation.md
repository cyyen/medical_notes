# Notebook on Various Medical Topics

[Dermatology]()

  * [Pruritus](dermatology/pruritus.md)

[Internal medicine]()

  * # Basic inpatient care
  * [Admission orders](internal_medicine/basic_inpatient_care/admission_orders.md)
  * [Blood glucose control](internal_medicine/basic_inpatient_care/blood_glucose_control.md)
  * # Chest medicine
  * [Community-acquired pneumonia](internal_medicine/chest_medicine/community-acquired_pneumonia.md)
  * [Dyspnea](internal_medicine/chest_medicine/dyspnea.md)
  * # Emergency medicine
  * [Rapid sequence intubation](internal_medicine/emergency/rapid_sequence_intubation.md)
  * # Hepatogastroenterology
  * [Albumin replenishment](internal_medicine/hepatogastroenterology/albumin_replenishment.md)
  * # Infectious diseases
  * [Amebiasis](internal_medicine/infectious_diseases/amebiasis.md)
  * [Cat-scratch disease](internal_medicine/infectious_diseases/cat_scratch_disease.md)
  * [Scrub typhus](internal_medicine/infectious_diseases/scrub_typhus.md)
  * # Oncology
  * [Hepatocellular carcinoma](internal_medicine/oncology/hepatocellular_carcinoma.md)
  * # Rheumatology
  * [Rheumatoid arthritis](internal_medicine/rheumatology/rheumatoid_arthritis.md)

[Neurology]()

  * [Acute disseminated encephalomyelitis (ADEM)](neurology/acute_disseminated_encephalomyelitis.md)
  * [Cushing's triad](neurology/cushings_triad.md)
  * [Multiple sclerosis (MS)](neurology/multiple_sclerosis.md)
  * [Neuromyelitis optica (NMO)](neurology/neuromyelitis_optica.md)

[Obstetrics and gynecology]()

  * # Obstetrics
  * [Hyperemesis gravidarum](obstetrics_and_gynecology/obstetrics/hyperemesis_gravidarum.md)
  * [Molar pregnancy](obstetrics_and_gynecology/obstetrics/molar_pregnancy.md)
  * # Gynecology 
  * [Pap smear](obstetrics_and_gynecology/gynecology/pap_smear.md)

[Pathology and laboratory medicine]()

  * # Hematology
  * [Blood drawing order](pathology_and_laboratory_medicine/hematology/blood_drawing_order.md)

[Pediatrics]()

  * # Pediatric neurology
  * [Febrile convulsion](pediatrics/febrile_convulsion.md)
  * # Pediatric cardiology
  * [Patent ductus arteriosus](pediatrics/patent_ductus_arteriosus.md)

[Pharmacology]()

  * # Analgesics
  * [Equianalgesic table](pharmacology/analgesics/equianalgesic_table.md)
  * # Antibiotics
  * [CNS penetration](pharmacology/antibiotics/cns_penetration.md)
  * # Antithrombotics
  * [Anticoagulants](pharmacology/antithrombotics/anticoagulants.md)
  * [Antiplatelet agents](pharmacology/antithrombotics/antiplatelet_agents.md)
  * # Immunosuppressants
  * [TNF antagonists](pharmacology/immunosuppressants/tnf_antagonists.md)

[About me](about.md)

<link rel="stylesheet" type="text/css" href="tipuesearch/css/tipuesearch.css">
<form action="search.html">
<div class="tipue_search_left"><img src="tipuesearch/search.png" class="tipue_search_icon"></div>
<div class="tipue_search_right"><input type="text" name="q" id="tipue_search_input" pattern=".{3,}" title="At least 3 characters" required></div>
<div style="clear: both;"></div>
</form>
