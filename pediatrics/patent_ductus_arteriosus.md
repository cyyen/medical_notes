# Patent ductus arteriosus (PDA)

## Treatment

* [Study](https://doi.org/10.1001/jama.2018.1896) suggests high dose of oral ibuprofen is more effective than intravenous indomethacin at closure of hemodynamically significant PDA in premature infants
