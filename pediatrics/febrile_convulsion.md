# Febrile convulsion

## Presentation
* Fever usually &gt; 39 degrees

## Epidemiology
* prevalence: 2-5%

## Types
* Simple (&lt; 15min)
* Complex (15-30min)
* Status epilepticus (&gt; 30min)

## Treatment
* Observation
  * No evidence supporting use of antipyretics or anticonvulsants
