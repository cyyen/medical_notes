Notebook on Various Medical Topics
==================================

Introduction
------------

This wiki (powered by [mdwiki](http://dynalon.github.io/mdwiki) contains my notes on a variety of medicine-related topics. There is no central theme, though there is a light slant towards internal medicine.

This wiki mostly exists as a group of Markdown files, to facilitate easy viewing using a web browser and viewing/editing using a text editor. A local search engine (Tipue Search) is integrated for users of the web version (though some fonts are hosted on the Google CDN); for local search please use grep and the like. The whole wiki is hosted on Gitlab as a Git repository; please feel free to clone it and use it as you wish (as long as the Creative Commons license is respected).

