# Rheumatoid arthritis

## Pathogenesis

### Role of anti-citrullinated protein antibodies (ACPA) [Lecture, May 25 2018]

* Environmental factors (smoking, air pollution, ...) ->
  production of citrullinated proteins ->
  deposition in joints and activation of osteoclastogenesis ->
  joint erosion and synovitis
  * cf. rheumatoid factor (RF), which does not activate osteoclastogenesis
  * ~80% of ACPA(+) patients are HLA-DB1(+)
* ACPAs undergo maturation (glycosylation of Fab and Fc sites) over the course of several years
  * Glycosylation reduces ACPA affinity for citrulin epitopes
  * ACPAs do not undergo further maturation after disease onset
* Subclinically detected ACPAs may be a good predictor of RA
* Concentration and number of subtypes positively correlated with severity
* Abatacept is effective at reducing ACPA concentrations
