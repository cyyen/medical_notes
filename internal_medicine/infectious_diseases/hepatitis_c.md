# Hepatitis C

## Genotypes

* 6 significant genotypes, with subtypes

## Treatment regimens

* Treatment regimen choice based on genotyping
* Drug types
  * protease inhibitors (PI): blocks NS3/4A protein
    * "-previr"
    * hepatic metabolism
    * examples: simeprevir, paritaprevir
  * NS5A inhibitor (NS5Ai)
    * "-asvir"
  * NS5B inhibitor (NS5Bi) (RNA polymerase inhibitor, RNAPi)
    * "-buvir"
    * renal excretion (no apparent nephrotoxic effects)
    * examples: sofosbuvir (Sovaldi)
* Combinations
  * glecaprevir/pibrentasvir (G/P) (Mavyret): PI + NS5Ai
  * ledipasvir/sofosbuvir (Harvoni): NS5Ai + NS5Bi
  * sofosbuvir/velpatasvir (Epclusa): NS5Ai + NS5Bi