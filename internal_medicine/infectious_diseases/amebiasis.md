# Amebiasis

## Etiology

* Infection by _Entamoeba histolytica_
* Spread by fecal-oral route (ingestion of amoebic cysts)

## Clinical presentation

* asymptomatic (90%)
* diarrhea (including dysentery)
* fever
* abdominal pain
* extra-enteric spread
  * hepatic (most common)
  * pulmonary
  * CNS

## Treatment

* tissue amebiasis: metronidazole (500-750mg iv tid * 5-10d)
* enteric amebiasis: paromomycin (500mg po tid * 10d)
