# Scrub typhus

## Etiology

* _Orientia tsutsugamushi_ spread via ticks

## Treatment

* Doxycycline 100mg bid PO/IV * 5 days 

## Prognosis

* Fatal if untreated (4-40%); 2% with antibiotic use
