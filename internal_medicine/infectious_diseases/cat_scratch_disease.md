# Cat-scratch disease

## Etiology

* Infection by _Bartonella henselae_
  * fastidious intracellular Gram(-) rod
  * natural reservoir: cats (especially kittens)
  * transmission among cats: cat fleas
* Transmitted to humans via cat bite or cat scratch wound contaminated by flea feces

## Clinical presentation

* lymphadenopathy (1-3 weeks post scratch)
  * lymphadenopathy may occur on side of neck or wound site
* fever
* malaise

### Clinical course

* Self-limited in majority of cases
* Disseminated disease (spread to CNS, liver, spleen) in 5-14%

## Diagnosis

* pathologic observation: granulomas containing intracellular rods on Warthin-Starry stain
* PCR

## Treatment

* azithromycin
* doxycycline (for CNS infection)

---

# tl;dr

* Observe for signs of disease (especially lymphadenopathy) 1-3 wks post wound
* azithromycin (500mg po stat + 250mg po qd * 4d) or doxycycline for treatment
