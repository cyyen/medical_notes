# Overview of antibiotics

## Anaerobes

* Anaerobes by location:

  * Enteric: Bacteroides, Prevotella
  
  * Oral: Fusobacterium, Peptostreptococcus
  
* Enteric anaerobes: less sensitive to clindamycin

## Drug resistance

* Mechanisms

  * penicillinase (7 classes), beta-lactamases (4 classes)
  
  * antibiotic target modification
  
  * wall thickening (e.g., in vancomycin-intermediate)
  
  * porin modification
  
* Novel beta-lactamase inhibitors: avibactam, etc.

* Ambler classification of beta-lactamases

  * A, B, C, D
  
    * A, C, D: targets serine core
  
    * B: targets zinc core
    
  * avibactam: targets class A
  
  * carbapenem resistance != carbapenemase resistance
  
  * bacteria can produce more than 1 type of beta lactamase