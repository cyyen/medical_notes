# Oropharyngeal and Esophageal Candidiasis

## Oropharyngeal candidiasis

* Local treatment in HIV-seronegative: nystatin 400000-600000 U QID
  * Note that nystatin contains sucrose to counteract taste; may increase risk of dental caries
  * clotrimazole, miconazole: not available as oral form at NCKUH
  * oral fluconazole if no response (200mg stat + 100-200mg qd * 7-14d)
* HIV-seropositive: oral fluconazole as above

## Esophageal candidiasis

* Systemic therapy only; _do not manage with local agents_
* IV fluconazole: 400mg stat + 200-400mg qd x 14-21d
* itraconazole, voriconazole: second-line
