# Delirium

## Definition

* Acute change in mental status
   * positive or negative symptoms
   * onset: 3-5d after admission

## Causes

* Biochemical imbalance
* Hyperdopaminergic / hypocholinergic activity
* Substance withdrawal
* Stroke

## Assessment

* CAM score
* CAM-ICU score
* MMSE

## Management

* Avoid anticholinergics and other drugs that are known to trigger delirium
* Correct underlying biochemicak imbalance
* Environment that relaxes and orients the patient
* Mechanical restraints
* Central oxygenation monitoring?
* Antipsychotics (taper and discontinue as soon as possible)
  * conventional: haloperidol
  * atypical: risperidone, olanzapine, quetiapine
