# Parenteral nutrition aids

## Simplified parenteral nutritional aids at NCKUH

Note: Values are *per container*. Amino acids (AA), glucose (Glu), lipids in grams, sodium (Na), potassium (K), calcium (Ca) in mEq.

| Name        | Volume (ml) |  Max dosage  | Calories | AA | Glu | lipids |  Na  | K   | Ca  |        Other contents         |  Notes                    |
|-------------|------------:|--------------|---------:|---:|----:|-------:|-----:|----:|----:|:------------------------------|:--------------------------|
| Clinimix    |    1500     | 40 ml/kg/day |    615   | 41 | 113 |   0    | 53   | 45  | 3.4 | Mg, Cl, P, acetate            | CVC only                  |
| Taita 5     |     400     |  1200 ml/day |    160   |  0 |  40 |   0    | 14.4 | 7.2 |  0  | Mg, Cl, P                     | peripheral (slow), high P |
| Smofkabiven |    1448     | 40 ml/kg/day |   1000   | 46 | 103 |  41    | 36   | 28  | 2.3 | Mg, Cl, P, Zn, S, acetate     | peripheral (slow)         |
| Bfluid      |    1000     |  2500 ml/day |    420   | 30 |  75 |   0    | 35   | 20  |  5  | Mg, Cl, P, Zn, S, vit. B1, ...| peripheral (slow)         |
| Paren-Aid 2 |     400     |  1200 ml/day |    400   |  0 | 100 |   0    | 12   | 12  |  0  | Mg, Cl, P, S, acetate         | CVC only                  |
| Paren-Aid 3 |     400     |  1200 ml/day |    400   |  0 | 100 |   0    | 12.2 | 12  |  8  | Mg, Cl, S, acetate, gluconate | CVC only                  |

## Holliday-Segar method of fluid and electrolyte requirement estimation

* Calories: 100-50-20 method (100 kcal/kg for 1st 10kg, 50 kcal/kg for 2nd 10kg, 20 kcal/kg afterwards)
* Electrolytes
  * Na: 3 mEq/100 kcal
  * K: 2 mEq/100 kcal
  * Cl: 2 mEq/100kcal
* Shortcut (65kg)
  * Calories: 2400 kcal
  * Na: 72 mEq
  * K: 48 mEq
  * Cl: 48 mEq