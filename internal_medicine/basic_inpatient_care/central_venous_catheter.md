# Central venous catheter (CVC)

## Indications for insertion

* rapid high volume infusion
* infusion of caustic drugs or hyperosmolar fluids
* measurement of central venous pressure

## Contraindications for insertion

* insertion site infection
* bleeding tendency (relative)

## Site selection

* common sites: internal jugular vein, subclavian vein, femoral vein
  * external jugular vein, axillary vein can also be used
* risk of infection: subclavian < internal jugular < femoral

## Insertion

### Depth

* right internal jugular vein: height(cm) / 10 - 1
* left internal jugular vein: height(cm) / 10 + 3~4
* femoral vein: no limit

## Steps

1. Preparation
  * protective gear: goggles, facial mask, hair cover, sterilized clothes cover, sterilized gloves, sterilized drapes
  * devices: CVC pack, guide wire
  * drugs: normal saline (for flushing catheter), lidocaine (local anesthesia), alcohol (sterilization), chlorhexidine (sterilization)
  * (ultrasound device)
2. Put on goggles, facial mask, hair cover; sterilize site with alcohol * 2, chlorhexidine * 1; put on sterilized clothes cover, sterilized gloves; apply drapes
3. Flush catheter with saline; leave caps open (guide wire exits from brown cap)
4. Local anesthesia
5. Locate vein (with ultrasound if necessary) and insert large-bore needle
6. Pass guide wire
7. Remove large-bore needle
8. Insert and apply dilator
9. Remove dilator
10. Insert CVC to desired length
11. Test CVC branches for patency with saline
12. Apply fixation device
13. Check CVC insertion depth with X-ray