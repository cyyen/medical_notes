# Blood glucose control

## Oral antihyperglycemic drugs

* Biguanides
  * metformin (risk of lactic acidosis in renal impairment (eGFR &lt; 30ml/min/1.73m^2; risk of vitamin B12 deficiency; glucose control and CV risk not improved in Type 1 DM)
* Secretagogues
  * Sulfonylureas: postprandial
  * Non-sulfonylurea secretagogues: preprandial
* DPP-4 inhibitors (for Type 2 DM)
* Thiazolidinediones (PPAR-gamma receptor agonists) (for Type 2 DM; increased risk of osteoporosis and heart failure?)
* Glycosurics (SGLT2 inhibitors) (increased risk of urinary tract infections and ketoacidosis)
* Alpha-glucosidase inhibitors

## Polypeptide analogs

* GLP-1 receptor agonists (e.g., liraglutide)
  * VARIATION study: basal + bolus insulin carries highest risk of hypoglycemia; basal + oral or GLP-1 receptor agonist (e.g. dulaglutide, IV form) carries lowest risk
  * Takes 2-4 weeks to stabilize blood glucose
* Amylin analogs (e.g., pramlintide; risk of amyloid formation; not available in Taiwan)

## Insulin

### Types of insulin

* Regular insulin (RI)
* Rapid-acting insulin
  * insulin aspart
* Intermediate-acting insulin
  * NPH insulin (neutral protamine Hagedorn): duration 10-16hr
* Slow-acting insulin
  * insulin glargine: duration 18-26h 
  * insulin detemir
* Insulin mixtures
  * NovoMix 70/30 (30% insulin aspart + 70% insulin aspart protamine)

### Insulin therapy

#### Dosage

##### Type 2 DM

* In insulin-naive Type 2 DM patients, start dose at 10 U or 0.1-0.2 U/kg/d; adjust 10-15% or 2-4 U QW or BIW to reach target
  * More aggressive protocols involve basing initial dose on BMI
  * If hypoglycemic and no reason could be determined, decrease 4 U or 10-20%
* If control unsatisfactory, add 1 rapid-acting insulin injection before largest meal or change to premixed insulin twice a day
  * Rapid-acting insulin: 4 U or 0.1 U/kg or 10% basal dose
  * Premixed insulin: divide into basal dose into 2/3 AM + 1/3 PM or 1/2 AM + 1/2 PM
* If still unsatisfactory, add rapid-acting injections before all meals, or change to premixed insulin 3 times a day

* Sliding scale: relatively poor glucose control; deprecated

#### Ratios between types of insulin

* Total daily insulin: 50% short- or rapid-acting + 50% long-acting [discussions at NCKUH]
  * e.g., Total daily dose 24 U -> 12 U short- or rapid-acting, 12 U long-acting
* Consider half-life of insulin product; 1 dose of long-acting insulin or 2-3 doses of intermediate-acting insulin, combined with pre- or post-prandial boluses of rapid-acting insulin
  * (cont.) e.g., (4 U NPH insulin + 4 U regular insulin) TID or (12 U insulin glargine QD + 4 U insulin aspart TID)

### Insulin sensitivity

*Note: only applicable to Type 1 DM*

* "1500 rule": insulin sensitivity (mg/dl glucose lowered per unit) for RI
  * insulin sensitivity = 1500 / total daily insulin
* "1800 rule": insulin sensitivity (mg/dl glucose lowered per unit) for rapid-acting insulin
  * insulin sensitivity = 1800 / total daily insulin

---

# tl;dr

* Type 1 DM
  * If insulin-naive:
    * "Count carbs" and test capillary glucose TIDAC, TIDPC, HS to determine insulin sensitivity
    * Start insulin (insulin glargine) at 10 U QD
* Type 2 DM
  * Initiate insulin therapy if HbA1c &ge; 10% or glucose &ge; 300mg/dl
  * Start insulin (insulin glargine) at 10 U QD
  * Oral antihyperglycemics: metformin 500mg TIDPC unless eGFR too low
    * Add another oral antihyperglycemic if HbA1c &ge; 9.0%
    * If patient is known to have atherosclerotic disease, second oral antihyperglycemic should be one known to lower risk, e.g., SGLT-1 inhibitor or GLP-1 agonist
