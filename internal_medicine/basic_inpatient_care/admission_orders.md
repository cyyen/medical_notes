# Admission orders

## Intravenous fluids

* Type and amount of IV fluid based on patient conditions
  * Solutions
    * Normal saline (0.9% NaCl): most common; may lead to hypernatremia in renal insufficiency; isotonic
    * Ringer's lactate: used in acidotic patients; isotonic
      * Ringer's acetate: avoids distorting lactate levels in sepsis
    * Taita No. 5: derivative of Ringer's lactate; contains calories
  * Daily requirement
    * 100-50-20 rule: daily requirement is 100ml for first 10kg, 50ml for second 10kg, and 20ml for each additional kg
    * 4-2-1 rule: hourly requirement is 4ml for first 10kg, 2ml for second 10kg, and 1ml for each additional kg

## Nutrition

### Caloric needs

* Calculate minimal caloric requirements based on patient characteristics:
  * basal energy expenditure: estimated in healthy subjects using Harris-Benedict equation; 70% of daily energy expenditure
    * ideal body weight (IBW)
    * height
    * age
  * patient stress (trauma, major surgery, severe illness, ...) determines multiplier
* Calculate minimum protein needs
  * 0.8-1.5 g/kg/day in normal patients
    * 0.8 g/kg/day satisfies requirements of 97% of adult population [Washington Manual 35e, p.33]
  * Increase protein intake in cases of stress, acute renal failure, and cirrhosis; decrease protein intake in chronic undialyzed renal failure
* In seriously ill or injured patients, start at &le; 50% of estimated energy and protein needs; build to full needs over 24-48 hrs [NICE Guidelines](https://www.nice.org.uk/guidance/cg32/chapter/1-Guidance)
* In patients that have been fasting for over 5 days, start at &le; 50% of estimated energy and protein needs to avoid refeeding syndrome; build to full needs over 48 hours [NICE Guidelines](https://www.nice.org.uk/guidance/cg32/chapter/1-Guidance)

### Electrolyte needs

* Limit sodium in cirrhotic patients
  * &le; 3g of table salt in mild to moderate cirrhosis; &le; 2g of table salt in severe cirrhosis
* Limit potassium in patients with renal deficiency

### Other diet considerations

* Diabetes mellitus: foods with low glycemic index
* Poor dentition: soft or liquid diet
* Vegetarian (including vegetarian diet on certain days)

## Other actions

* Continue outpatient medication unless contraindicated
* Check vitals (T/P/R, BP, SpO2, consciousness) routinely
  * Ward routine: 0900, 1400, 2100
  * Increase frequency as needed (e.g., in unstable victims)
    * Order multi-monitor usage if necessary
* Check muscle power (in neurologic disease) routinely
  * Increase frequency as needed (e.g., in recent stroke)
* Check input/output, body weight (especially in heart failure and renal insufficiency)
  * Amount of food/fluid intake, urine/fecal output
* Wound/catheter care as needed

---

# tl;dr

* Admission order: name of VS, diagnosis
* IV fluid: usually normal saline (0.9% NaCl) @ 2500ml QD for 70kg; 1 bag (500ml) QD if only used as IV carrier
  * 2500ml QD corresponds to 22.5g NaCl QD
* Diet orders
* Continue outpatient medication unless contraindicated
* Check vitals (T/P/R, BP, SpO2, consciousness) 
