# Rapid sequence intubation (RSI)

## Indications

* risk of pulmonary aspiration
* impending airway compromise

## Medications

NCKUH protocol:

* lidocaine 1.5mg/kg
* etomidate 0.3mg/kg
* rocuronium 0.6mg/kg

## Technique

* Preparation
  * placement of monitoring equipment
    * check patient condition and evaluate difficulty of intubation (jaw opening width, Mallampati score, etc.)
  * IV access: 2 peripheral lines
  * equipment preparation: laryngoscope, drugs, endotracheal tube
    * check laryngoscope batteries and select properly-sized blade
    * endotracheal tube insertion length: 22-24cm from teeth
* Preoxygenation
  * use of pure oxygen to wash out nitrogen from functional residual capacity
  * target: SpO2 at 90% for over 8 min
* Pretreatment
  * medications given to high-risk groups
    * lidocaine 1.5mg/kg
      * suppresses cough reflex, prevents rise in intracranial pressure (weak evidence)
    * atropine 0.01-0.02mg/kg (not routinely given)
      * anticholinergic agent
* Paralysis with induction
  * opioid, hypnotic and paralytic agent given in rapid succession
    * opioid as analgesic to suppress sympathetic response; may exacerbate respiratory depression
    * commonly used hypnotics: sodium thiopental, propofol, etomidate
      * etomidate 0.3mg/kg
        * most hemodynamically neutral of the commonly used hypnotics
        * risk of adrenal suppression if used chronically
    * commonly used paralytic agents: succinylcholine (depolarizing), cisatracurium (nondepolarizing), rocuronium (nondepolarizing)
      * rocuronium 0.6mg/kg
      * succinylcholine 1.5mg/kg
        * can cause hyperkalemia
        * contraindicated in patients with history of malignant hyperthermia, reduced renal function, burns, or major trauma
* Positioning
  * "sniffing" position (neck flexion + head extension)
* Placement of tube
* Postintubation management
  * check for end-tidal CO2 and bilateral chest rise
  * auscultate stomach
  * order portable chest X-ray if necessary