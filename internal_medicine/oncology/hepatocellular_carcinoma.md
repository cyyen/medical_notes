# Hepatocellular carcinoma

## Risk factors
* HBV / HCV cirrhosis (in 80%)
* Hereditary hemochromatosis
* Wilson's disease

## Diagnosis
* Screen q6m in hepatitis B patients over 40 y/o
  * Ultrasound +/- alpha-fetoprotein

## Treatment
* Resection (if single lesion &lt; 2cm and Child-Pugh class A w/o portal hypertension)
* Radiofrequency ablation (if tumor &lt; 3cm in size)
* Transarterial chemoembolization (TACE) for large tumors or if not amenable to radiofrequency ablation
* Liver transplant if patient matches Milan criteria (up to 3 tumors &le; 3cm in size each *or* single tumor &le; 5cm in size)
* Sorafenib (angiogenesis inhibitor) for advanced HCC; modest advantage over supportive care
  * Minimal dose: 2 tabs qd; maximal dose: 6 tabs qd
    * Adverse effects limit ethnic Asians to ~4 tabs qd
* Immunotherapy
  * nivolumab
    * risk of damage to brain, heart, and lungs
    * success rate: 15-20%
      * in conjunction with sorafenib: 30%
    * injection q2w
    * f/u imaging every 6 months
  * pembrolizumab
    * injection q3w, with drug holidays
  * ramucirumab (off-label) 
    * injection q2w

### Chemotherapy modalities

* Transarterial chemoembolization (TACE)
  * reduced effect if tumor is located in portal vein (with portal vein thrombosis)
  * higher local drug concentration (20x that of bloodstream concentration)
* Hepatic arterial infusion chemotherapy (HAIC)
  * requires higher drug concentration than TACE
  * better effect than TACE for portal vein thrombosis
    * HCC drainage: hepatic veins in early stage, sinusoids in middle stage, portal vein in late stage
* Systemic chemotherapy

### Chemotherapy drugs

* doxorubicin
  * cardiotoxicity
  * accumulation effect (may lead to heart failure after accumulated dose of around 200mg/kg)
