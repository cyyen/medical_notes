# Cutaneous T cell lymphoma (CTCL)

## Common subtypes

* mycosis fungoides (MF) (&ge; 50%)
* Sezary syndrome (SS)
* ...

## Distinction

* Mycosis fungoides: skin involvement of cancerous T cells
  * Mycosis fungoides with large cell transformation: large cells (&ge; 4x size of normal lymphocyte) in &ge; 25% or dermal infiltrate [https://doi.org/10.1016/j.jaad.2013.09.007]
* Sezary syndrome: skin and leukemic involvement of T cells
* Unknown if Sezary syndrome is an advanced form of mycosis fungoides or a separate disease [Cleveland Clinic]

## Clinical presentation

* Rash-like patches, tumors or lesions

## Diagnosis

* Mycosis fungoides: Pautrier microabscesses (&ge; 4 atypical lymphocytes arranged in epidermis) (pathognomic)

## Treatment

* sunlight, UVB (312nm)
* topical steroids
* chemotherapy
* superficial radiotherapy
* histone deacetylase inhibitors, e.g., vorinostat
* retinoids, e.g., bexarotene
* biologics, e.g., brentuximab vedotin (CD30-targeting antibody-drug conjugate)