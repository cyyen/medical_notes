# Tumor lysis syndrome

## Etiology

* Massive tumor cell lysis
  * Most common in aggressive lymphoma and ALL
* Hyperkalemia, hyperphosphatemia, hyperuricemia, hypocalcemia
  * leads to tubule damage and AKI
  * Cairo-Bishop definition: 25% change from baseline in K, uric acid, P, K

## Prophylaxis

* Aggressive hydration
  * 2-3 L/m2BSA/day of D5W+0.25% saline or 0.9% N/S
    * avoid fluids containing K or Ca (risk of calcium phosphate precipitation)
* Hypouricemic agents
  * allopurinol (100mg/m2 q8h; max 800mg qd)
  * rasburicase (0.2 mg/kg qd x 5-7d)
    * _rasburicase contraindicated in G6PD deficiency_