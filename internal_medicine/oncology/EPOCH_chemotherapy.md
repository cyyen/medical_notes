# EPOCH chemotherapy

## Components, dose, and days of administration

* Rituximab
  * 375 mg/m2 IV
  * day 0 or 1
* Etoposide
  * 50 mg/m2 IV QD (cont. infusion)
  * day 1-4
* Prednisone
  * 60 mg/m2 PO BID
  * day 1-5
* Oncovin (vincristine)
  * 0.4 mg/m2 IV QD (cont. infusion)
  * day 1-4
* Cyclophosphamide
  * 750 mg/m2 IV
  * day 5
* Hydroxydaunorubicin (doxorubicin)
  * 10 mg/m2 IV QD (cont. infusion)
  * day 1-4
* Granulocyte colony-stimulating factor
  * day 6

## Dose adjustment

* Escalation
  * may be above starting dose: etoposide, doxorubicin, cyclophosphamide
  * increase all by 20% if nadir ANC &gt; 500/uL
* Deescalation
  * may be below starting dose: cyclophosphamide _only_
  * etoposide, doxorubicin, cyclophosphamide: decrease all by 20% if nadir ANC &lt; 500 uL for over 10 days or if nadir platelet count &lt; 25000/uL 
* Maintain
  * nadir ANC &lt; 500/uL on 1 or 2 checks but &gt; 500/uL at 3rd check and nadir platelet count &gt; 25000/uL
