# Non-Small Cell Lung Cancer

## Treatment

### Stage I-II

* Surgical resection
  * procedure of choice: lobectomy
* Radiotherapy
* Adjuvant chemotherapy

### Stage III

* Complete resection (if feasible) + adjuvant chemotherapy
* Concurrent chemoradiotherapy if complete resection not feasible

### Stage IV

* Check PD-L1 expression, driver mutations (EGFR, ALK, ROS1, BRAF), disease extent, histology (squamous vs. nonsquamous)
  * PD-L1 expression &gt; 50%: pembrolizumab monotherapy
  * PD-L1 expression &lt; 50%: pembrolizumab + double chemotherapy
  * if driver mutation present: select appropriate target therapy
* Systemic chemotherapy if performance status is poor (e.g., ECOG 3-4)
* Palliative radiotherapy (short course)
