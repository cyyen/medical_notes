# Peritoneal dialysis

## Selection of peritoneal dialysis regimen

### Peritoenal Equilibration Test (PET)

* Reproducible way of evaluating peritoneal transport function

### Classification of peritoneal membrane function

cf. UpToDate, "Peritoneal equilibration test"

Note:
* D/P ratio: dialysate-to-plasma ratio
* D/Do ratio: ratio of current level to initial level

----

* High transporter (or fast transporter) – Defined as a creatinine D/P greater than +1 standard deviation (SD) from the mean or a glucose D/Do of less than -1 SD from the mean
* Low transporter (or slow transporter) – Defined as a creatinine D/P of less than -1 SD from the mean or a glucose D/Do of greater than +1 SD from the mean
* Average transporter – Defined as a creatinine D/P and a glucose D/Do of between +1 SD and -1 SD around the mean

### Ultrafiltration failure

* Modified PET: 2.5% dextrose replaced with 4.25% dextrose
* Failure defined as UF volume of less than 400mL after 4 hour dwell with 2 liters of 4.25% dextrose