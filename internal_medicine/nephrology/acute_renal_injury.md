# Acute kidney injury (AKI)

## Definition

* increase in Cr &ge; 0.3mg/dl or &ge; 50% within 48hr
* urine output &lt; 0.5ml/kg/hr for &ge; 6hr: disputed

## Workup

  * BUN/creatinine ratio not a reliable indicator of pre-renal AKI [BMC Nephrol. 2017; 18: 173.]
    * Functional excretion of sodium (FENa) (or functional excretion of urea (FEN)) should be used instead
  * Pre-renal causes: insufficient blood supply to kidneys
    * low effective arterial volume: hypovolemia, heart failure, vasodilation, third-spacing
    * renal vasoconstriction: drugs (NSAIDs, ACEI/ARB, contrast media), hypercalcemia
    * large vessel-related: renal artery stenosis, abdominal compartment syndrome, venous thromboembolism
  * Intrinsic causes: direct damage to kidney tubules
  * Post-renal causes: obstruction of urine output
