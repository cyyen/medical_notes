# Ascites

## Tests

* acid-fast stain: very low sensitivity for TB peritonitis
* Hb
* cytology
* ascites routine

## Diagnostic steps (lecture by Dr. Chien Shih-Chie, cf. UpToDate)

1. Color: colorless, red, milky white, tea-colored
  * colorless: portal hypertension, congestive heart failure, infection, ...
  * blood-red: hemoperitoneum (RBC &gt; 50k) (malignancy, traumatic tapping, ...)
    * determining if hemoperitoneum is caused by traumatic tapping: let sample stand and observe if clots form; if clots form, blood is fresh; if no clots form, hemoperitoneum has present prior to tapping (intraabdominal clots have formed and coagulation factors have been expended)
  * milky white: chylothorax (malignancy, TB, trauma to lymphatic duct, spontaneous)
    * triglyceride (TG) &gt; 220
  * tea-colored: bile leak
    * ascites-serum total bilirubin ratio &gt; 0.6
2. Serum-ascites albumin ratio (SAAG)
  * &ge; 1.1: hydrostatic pressure-related
    * congestive heart disease
    * cirrhosis
  * &lt; 1.1: capillary leak-related
    * infection
      * tuberculosis
      * bacterial peritonitis: polymorphous neutrophil (PMN) count &ge; 250
        * spontaneous bacterial peritonitis
	* secondary bacterial peritonitis
    * malignancy
    * nephrotic syndrome
