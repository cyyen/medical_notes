# Albumin replenishment

## Indications

Note: focus should be on treating underlying cause, rather than replenishment per se

* Uncontrolled ascites
* Major trauma or burns

## Other considerations

* Normal albumin level: 3.5-5g/dl
  * replenishment goal: 3g/dl
* Coverage by Taiwanese National Health Insurance:
  * &le; 2.5g/dl: partial coverage
  * &le; 2.0g/dl: total coverage
* Injection rate should be no more than 50ml/day to avoid fluid overload
