# PT prolongation

## Etiology

* Oral anticoagulant overdose (vitamin K antagonists)
* Poor hepatic function
  * reduced production of vitamin K-dependent coagulation factors

## Treatment

* Fresh frozen plasma (FFP)
  * maintenance: hours
  * reduced effect if patient INR is between 1.5 to 1.7 (FFP itself has INR of 1.7)
* vitamin K
  * time to effect: 6-12 hr