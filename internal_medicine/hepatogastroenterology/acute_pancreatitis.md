# Acute pancreatitis

## Management

### Hydration

* IV hydration: with normal saline or Ringer's lactate
  * _Ringer's lactate contraindicated in hypercalcemia!_
* Time: initial 24-48 hr
* Rate:
  * 5-10 ml/kg/hr
  * If severe volume depletion: load 20 ml/kg over 30min, then 3ml/kg/hr for 8-12hr
  * more commonly seen at NCKUH: load 20 ml/kg over 30min, then adjust according to status
    * check BUN 24hr later
    * NCKUH: follow creatinine q8h-q12h

### Pain control

* morphine, fentanyl, or hydromorphone

### Nutrition

* If no ileus or N/V, early oral feeding is acceptable if patient is hungry
  * select high-protein, low-fat diet
* If severe pancreatitis, feed from nasogastric or nasojejunal tube
* Parenteral nutrition only if patient cannot tolerate feeding at all or if target feeding rate cannot be achieved within 48-72 hr

### Antibiotics

* No indication for routine antibiotic use even in severe pancreatitis

### Imaging

* CT not always indicated (especially if surgical intervention not immediately indicated)

### Protease inhibitors

* No evidence of utility of gabexate (trade name: Foy, serine protease inhibitor)
  * Indicated in Japanese guidelines but not in AGA guidelines

### Surgical intervention

#### Infected necrosis

* Try minimally invasive techniques (e.g., ERCP) before considering necrosectomy
* Necrosectomy 4-6 weeks later (necrosis is walled off and easier to manage)

#### Sterile necrosis

* Intervention only if obstruction due to mass effect or continuous symptoms after 8 weeks