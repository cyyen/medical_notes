# Dyspnea

* Low-dose morphine is safe in management of dyspnea
* Proglumide (a CCK antagonist) has been shown to enhance the analgesic effects of morphine and reduce drug tolerance
