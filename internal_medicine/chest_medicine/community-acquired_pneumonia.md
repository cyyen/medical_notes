# Community-acquired pneumonia
### Lecture at NTUH, May 4, 2017

* Legionella
  * atypical pneumonia
  * rapid progression
* sputum sample
  * satisfactory sample
    * &gt; 25 polymorphonuclear leukocytes (PMNs) / low-power field (LPF)
    * &lt; 10 epithelial cells / LPF
  * _Candida_: part of normal flora in healthy subjects; no need for treatment except in immunocompromised patients
* antibiotic resistance in anaerobes: due to beta-lactamase
* ceftriaxone: less action against anaerobes
* quinolones
  * ciprofloxacin
  * levofloxacin
  * moxifloxacin
