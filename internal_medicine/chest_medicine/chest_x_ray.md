# Interpretation of chest X-rays (lecture series by Dr. Chang Han-Yu)

## Signs found on chest X-rays

* air-crescent sign
  * collapse (cavitation) and refilling
    * "ball in hole" (not freely movable)
* lung collapse
  *  only becomes more dense if >50% of lung is collapsed
* unilateral hilar enlargement
  * less likely to be vessel
* hilar converging sign

## Disease with special chest X-ray findings and considerations

* Osler-Weber-Rendu syndrome (hereditary hemorrhagic telangiectasia)
  * lung nodules (hemangiomas)
* aspergilloma
  * fungal ball may cause bronchial artery bleeding; TAE (coil insertion) needed
* lower lobe pneumonia
  * may present as upper abdominal pain
* endobronchial tuberculosis
  * may cause atelectasis and lobar collapse
    * sharp lesion border
    * lack of lung markings