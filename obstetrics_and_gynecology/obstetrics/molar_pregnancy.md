# Molar pregnancy
* Mole types: complete, incomplete
  * Complete mole: 2 haploid sperms + 1 empty egg, or 1 duplicated haploid sperm + 1 empty egg
  * Incomplete mole: &ge; 2 haploid sperms + 1 haploid egg
* Moles with dual sperm methylation pattern: overgrowth of syncytiotrophoblast; moles with dual egg methylation pattern: underdevelopment of syncytiotrophoblast 
