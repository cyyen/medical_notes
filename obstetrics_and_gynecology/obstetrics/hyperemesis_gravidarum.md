# Hyperemesis gravidarum

## Etiology

* Unknown; may be related to changes in levels in hCG

## Clinical course

* Usually peaks at 4-16 weeks pregnant but can persist through pregnancy

## Treatment

* Antiemetics
  * antihistamines
    * pyridoxine/doxylamine: pregnancy category A
      * doxylamine: 1st-generation antihistamine
  * dopamine receptor antagonists
    * metoclopramide, etc.
    * may cause extrapyramidal symptoms; counter with antihistamines (e.g., diphenhydramine)
  * NK-1 inhibitor
  * serotonin receptor antagonists
* Rehydration
  * replenishment of pyridoxine
