# Indication for ICU stay post-surgery

## Failure of ventilator weaning

* Poor cardiovascular / pulmonary function
  * interstitial lung diseases
* Old age
* Facial trauma