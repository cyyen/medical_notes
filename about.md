# About me

My name is Chang-Yi Yen. I am an M.D. from Taiwan with a side interest in information technology. This is my attempt to gather some notes related to medicine that I have made over the past few years into an easily-hosted, easily-accessible format. Please do *not* take the contents of this site as any form of medical advice; consult your doctor for that.

All content on this site is published under the Creative Commons Attribution Share-Alike License (CC BY-SA).

I can be reached at changyiyen AT gmail DOT com.
