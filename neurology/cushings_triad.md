# Cushing's triad

## Definition

* systolic hypertension
  * Activation of the sympathetic nervous system to restore cerebral blood flow
* reflex bradycardia
  * Activation of the parasympathetic nervous system in response to hypertension; also causes Cushing ulcers
* slow, irregular respiration
  * Compression of brain structures

## Etiology

* Ischemia of structures located in the posterior cranial fossa
