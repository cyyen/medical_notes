# Stroke

## Ischemic stroke

* Stroke risk after transient ischemic attack: calculate ABCD2 score (age, BP, clinical features, duration, DM)
* Assessment
  * Na, K, Cr, glucose, CBC, PT/aPTT
  * stat head CT (_for ruling out ICH_)
    * CT angiography for endovascular thrombectomy
* Treatment
  * thrombolysis (tPA 0.9 mg/kg, max 90 mg)
    * "Door-to-needle time" is canonically 60 min, but preferably ASAP
    * Consider 0.6 mg/kg in Asians
  * BP control
    * medication: nicardipine, labetalol
    * pre-lysis goal: 185/110 mmHg
    * post-lysis goal: 180/105 mmHg
  * prevention of cerebral edema
  * endovascular thrombectomy
* Secondary prevention
  * antiplatelet therapy
    * (aspirin + clopidogrel) x 90d
  * anticoagulation
    * _only if_ AF, hypercoagulative state, paradoxical emboli (except bacterial endocarditis)
  * BP control
  * statins
  * pioglitazone in insulin-resistant patients

## Hemorrhagic stroke

* Assessment
  * stat head CT
  * CT or conventional angiography
  * PT/aPTT
* Management
  * reverse coagulopathy
    * goal INR &gt; 1.4, Plt &gt; 100k
    * no Plt transfusion if on antiplatelet meds
    * DDAVP if uremic
  * BP control
    * goal SBP &lt; 160 mmHg
    * nicardipine or labetalol
  * endovascular coiling
  * surgical evacuation
  * seizure prophylaxis
