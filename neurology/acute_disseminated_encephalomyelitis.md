# Acute disseminated encephalomyelitis (ADEM)

## Etiology

* Autoimmune demyelinating disease post viral or bacterial infection, or nonroutine vaccination
  * cf. Guillain-Barre syndrome?

## Epidemiology

* Prevalence: 8 / 1,000,000
* Occurs more often in children

## Clinical course

* Single flare-up, as opposed to multiple sclerosis (MS), which is characterized by multiple relapses
  * Symptoms begin 1-3 weeks post infection
* Rapid worsening of symptoms over hours to days
  * Symptoms: fever, headache, nausea, vomiting, confusion, visual impairment, seizures, coma, etc.
  * Severity peaks at 4.5 days
* Average time to recovery: 1-6 months

### Acute hemorrhagic leukoencephalitis

* Hyperacute form of ADEM
* Perivenular necrosis and hemorrhage
* Mortality: 70%

## Pathology

* Perivenular "sleeves" of demyelination 

## Treatment

* High dose IV glucocorticoids
  * Methylprednisolone more effective than dexamethasone 
* Plasmapheresis
* Intravenous immunoglobulins (IVIG)

## Prognosis

* Mortality rate: 5-10%
* Full recovery: 50-75%
