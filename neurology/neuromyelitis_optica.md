# Neuromyelitis optica (NMO)

## Diagnosis

### Mayo Clinic criteria (2015)

Two absolute + 2 of 3 supportive criteria:

* Absolute criteria
  * optic neuritis
  * acute myelitis
* Supportive criteria
  * brain MRI showing lesions not meeting MS criteria
  * spinal cord MRI showing lesion &ge; 3 vertebrae in size
  * anti-aquaporin 4 autoantibody
